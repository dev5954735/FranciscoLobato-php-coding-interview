<?php

namespace Src\controllers;

use Src\helpers\Helpers;
use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking($booking) {
		$clients = new Client();

		$allClients = $clients->getClients();
		$client = Helpers::arraySearchI($booking['client']['email'], $allClients, "email");

		if(!$client) {
			$newClient = $clients->createClient($booking['client']);

			$client = $newClient;
		} else {
			$client = $allClients[$client];
		}
		unset($booking['client']);

		$check = $this->checkDiscount($client['id']);

		if($check) {
			$booking['price'] = $booking['price'] * 0.9;
		}

		$booking['client_id'] = $client['id'];

		return $this->getBookingModel()->createBooking($booking);
	}

	private function checkDiscount($client_id) {
		$dogs = new Dog();
		$allDogs = $dogs->getDogs();

		$sum = $average = 0;
		$clientDogs = [];

		foreach($allDogs as $d) {
			if($d['clientid'] == $client_id) {
				$clientDogs[] = $d;
				$sum = $d['age'];
			}
		}

		if(count($clientDogs) > 0 ) {
			$average = $sum / count($clientDogs);
		}

		if($average < 10 && $average != 0) {
			return true;
		}

		return false;
	}
}