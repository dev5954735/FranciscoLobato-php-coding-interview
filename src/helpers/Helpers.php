<?php

namespace Src\helpers;

use function PHPUnit\Framework\isEmpty;

class Helpers {
	function putJson($users, $entity) {
		file_put_contents(dirname(__DIR__) . '/../scripts/'.$entity.'.json', json_encode($users, JSON_PRETTY_PRINT));
	}
	
	public static function arraySearchI($needle, $haystack, $column) {
		return array_search($needle, array_column($haystack, $column));
	}

	public static function validatePhone(string $phoneNumber, string $countryCode = NULL, $format = 1) {
		if(isEmpty($phoneNumber)) {
			return false;
		} 

		$url = "http://apilayer.net/api/validate?";
		$url .= "access_key=0e7f41f523b710dc531bad330f7f4b8f";
		$url .= "&number=" . $phoneNumber;
		
		if(!isEmpty($countryCode)) {
			$url .= "&country_code=" . $countryCode; 
		}

		$url = "&format=" . $format;

		$validate = Helpers::callAPI("POST", $url);

		return $validate['valid'];

	}

	public static function callAPI($method, $url, $data = false) {
		$curl = curl_init();

		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
	
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}
	
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	
		$result = curl_exec($curl);
	
		curl_close($curl);

		$result = json_decode($result, true);
		
		return $result;
	}
}